<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\Produit;
use DB;

class ProduitsController extends Controller
{ 
    /**
    * Create a new controller instance.
    *
    * @return void
    */
   public function __construct()
   {
       $this->middleware('auth');
   }

   /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */

    public function index()
    {
        $produits = Produit::latest()->paginate(5);
        return view('produits.index',compact('produits'))->with('i', (request()->input('page', 1) - 1) * 5);
    }

    public function create()
    {
        return view('produits.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:produits',
            'quantite' => 'required|unique:produits',
            'prix' => 'required',
        ]);
  
        Produit::create($request->all());
            
        return redirect()->route('indexpro')->with('success','Produit created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /*public function show($id=null)
    {
        return view('produits.show',compact('id'));   
    }*/

    public function show($produit)
    {
        $produit = Produit::find($produit);
        return view('produits.show',compact('produit'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function edit($produit)
    {
        $produit = Produit::find($produit);
        return view('produits.edit', compact('produit'));
    }

    public function update($produit)
    {
        $request->validate([
            'name' => 'required',
            'quantite' => 'required',
            'prix' => 'required',
        ]);
  
        $produit->update($request->all());
  
        return redirect()->route('indexpro')->with('success','Produit updated successfully');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    

    public function destroy(Produit $produit)
    {
        $produit->delete();
  
        return redirect()->route('indexpro')->with('success','Produit deleted successfully');
    }

    public function about()
    {
        return view('produits.about');
    }

    // vehicle

    
    public function createvehicle(Request $request)
    {
        $request->validate([
            'assign_vehicle_title' => 'required',
            'Fare' => 'required',
        ]);
  
        Produit::create($request->all());
            
        return redirect()->route('about')->with('success','Vehicle created successfully.');
    }





}
