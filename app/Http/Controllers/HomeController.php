<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\Student;
use DB;
use Validator;
use App\Operator;

class HomeController extends Controller


{
    public function __construct()
   {
       $this->middleware('auth');
   }


    public function index()
    {
        return view ('home');
    }


    public function indexStudent()
    {
        $students = Student::get();

        return view('student.index', compact('students'));
    }

    /**
     * Create a new student
     */
     public function createStudent(Request $request, Student $student)
    {
        Validator::make($request->all(), [
            'quantite' => 'required','unique:students,quantite,'.$student->id,
            'prix'      => 'required|string|max:255',
            
          ])->validate();

        Student::create([
            'quantite' => $request->get('quantite'),
            'prix' => $request->get('prix'),
            
        ]);
        
        return redirect()->back()->with('success', 'Student Added Successfully');
    }

    /**
     * Update the student
     */
     public function updateStudent(Request $request, Student $student, $id)
    {
        Validator::make($request->all(), [
            'quantite' => 'required','unique:students,quantite,',
            'prix'      => 'required|string|max:255',
            
          ])->validate();

        $academic_years = Student::where('id', $id)->update([
            'quantite' => $request->get('quantite'),
            'prix' => $request->get('prix'),
            
        ]);
        return redirect()->back()->with('success', 'Student updated Successfully');
    }
    public function update($produit)
    {
        $request->validate([
            'name' => 'required',
            'quantite' => 'required',
            'prix' => 'required',
        ]);
  
        $produit->update($request->all());
  
        return redirect()->route('indexpro')->with('success','Produit updated successfully');
    }











     public function deleteStudent($id)
    {
        $student = Student::find($id);
        $student->delete();

        return redirect()->back()->with('success', 'Student deleted Successfully');
    }


}

