<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@yield('title')</title>

        <!-- Scripts -->
        
        <script src="/assets/js/vendor/jquery-3.3.1.min.js"></script>
        <script src="/assets/js/vendor/bootstrap.bundle.min.js"></script>
        <script src="/assets/js/vendor/perfect-scrollbar.min.js"></script>
        <script src="/assets/js/vendor/echarts.min.js"></script>

        <script src="/assets/js/vendor/sweetalert2.min.js"></script>
        <script src="/assets/js/es5/script.min.js"></script>
        <script src="/assets/js/sweetalert.script.js"></script>

        <script src="/assets/js/es5/echart.options.min.js"></script>
        <script src="/assets/js/es5/dashboard.v1.script.min.js"></script>
        <script src="/assets/js/es5/script.min.js"></script>
        <!-- page vendor js -->
        <script src="/assets/js/vendor/datatables.min.js"></script>
        <script src="/assets/js/es5/script.min.js"></script>
        <!-- page custom js -->
        <script src="/assets/js/datatables.script.js"></script>
            <script>
  function initFreshChat() {
    window.fcWidget.init({
      token: "90c3afa0-c2d9-4ab6-9b7a-a27c30a5feeb",
      host: "https://wchat.freshchat.com"
    });
  }
  function initialize(i,t){var e;i.getElementById(t)?initFreshChat():((e=i.createElement("script")).id=t,e.async=!0,e.src="https://wchat.freshchat.com/js/widget.js",e.onload=initFreshChat,i.head.appendChild(e))}function initiateCall(){initialize(document,"freshchat-js-sdk")}window.addEventListener?window.addEventListener("load",initiateCall,!1):window.attachEvent("load",initiateCall,!1);
</script>

        <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js" charset="utf-8"></script> -->
       
        <!-- charts -->
        <script src="/js/chart.min.js"></script>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,400i,600,700,800,900" rel="stylesheet">
    
        <!-- Styles -->
        <link rel="stylesheet" href="/assets/styles/css/themes/lite-purple.min.css">
        <link rel="stylesheet" href="/assets/styles/vendor/perfect-scrollbar.css">
        <link rel="stylesheet" href="/assets/styles/vendor/perfect-scrollbar.css">
        <link rel="stylesheet" href="/assets/styles/vendor/datatables.min.css">

        <link rel="stylesheet" href="/assets/styles/vendor/perfect-scrollbar.css">
        <link rel="stylesheet" href="/assets/styles/vendor/sweetalert2.min.css">

       

    </head>

    <body>

        @include('inc.ministry.headerMin')

        @include('inc.ministry.sidenav_min')
        
        @include('inc.settings')

        <!-- @include('inc.messages') -->
        
        @yield('content')
    
    </body>

</html>