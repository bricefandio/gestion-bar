@extends('layouts.app')
  
@section('content')


<div class="content">
    <div class="container-fluid">
      <!-- Small boxes (Stat box) -->
      <div class="row">
       <div class="col-md-12 col-sm-12">
          <div class="card card-info card-outline card-tabs">
          @include('inc.messages')
            <div class="card-body">
                <div class="row">
        <!-- left column -->
        <div class="col-md-4">
          <!-- general form elements -->
          <div class="card card-info">
           <div class="card-header">
              <h3 class="card-title">Add Transport Assign Vehicle</h3>
               <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                </div>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" method="post" action="">
            @csrf
            <div class="card-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Assign Vehicle Title</label>
                  <input type="text" class="form-control" name="assign_vehicle_title" id="exampleInputEmail1" placeholder="Enter Assign Vehicle Title">
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1">Fare</label>
                  <input type="text" class="form-control" name="fare" id="exampleInputEmail1" placeholder="Enter Fare">
                </div>
                
              </div>
              <!-- /.card-body -->
              <input type="hidden" value="1" name="board_id">
             <input type="hidden" value="Baptist Board" name="board_name">
              <div class="card-footer">
                <span class="float-right"> <button type="submit" class="btn btn-info"><i
                          class="fas fa-plus"></i>
                      Add Assign Vehicle</button></span>
              </div>
            </form>
          <!-- /.card -->
        

        </div>
</div>
        <!--/.col (left) -->
        <!-- right column -->
        <div class="col-md-8">
        <div class="card card-info">
            <div class="card-header">
              <h3 class="card-title">View Transport Assign Vehicle</h3>
               <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table class="table table-bordered table-hover dataTable table">
                  <thead class="thead-light">
                      <tr>
                          <th>Assign Vehicle Title</th>
                          <th>Fare</th>
                          <th>Action</th>
                      </tr>
                  </thead>
                  <tbody>
                     <tr>
                      <td> </td>
                      <td> </td>
                      <td> </td>
                       
                      </tr>
                      
                  </tbody>
                  <tfoot class="tfoot-light">
                  <tr>
                  
                      <th>Assign Vehicle Title</th>
                      <th>Fare</th>
                      <th>Action</th>
                      </tr>
                  </tfoot>
              </table>
            </div>
        </div>
        

         
                 </div>
             
            <!-- /.card -->
          </div>
        </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
    <!-- Main content -->
    
<!-- /.card-body -->
        </div>
      </div>
    </div>
  
    </div>
    <!-- /.card -->
    </div></div>

      
  <!-- /.content -->
</div>

<!-- /.content-wrapper -->
@endsection

  @section('special-scripts')

<script type="text/javascript">
  $('#region').change(function(){
  var regionID = $(this).val();
  if(regionID){
      $.ajax({
         type:"GET",
         url:"{{route('about')}}?region_id="+regionID,
         success:function(res){
          if(res){
              $("#division").empty();
              $("#division").append('<option value="">Select</option>');
              $.each(res,function(key,value){
                  $("#division").append('<option value="'+key+'">'+value+'</option>');
              });

          }else{
             $("#division").empty();
          }
         }
      });
  }else{
      $("#division").empty();
      $("#subdivision").empty();
  }
 });
  
</script>
    @endsection