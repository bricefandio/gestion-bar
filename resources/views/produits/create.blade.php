@extends('layouts.app')
  
@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Add New Produit</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('indexpro') }}"> Back</a>
        </div>
    </div>
</div>
   
@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
   
<form action="{{ route('storepro') }}" method="POST">
    @csrf
  
     <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong><label for="exampleInputEmail1">Name</label></strong>
                <input type="text" name="name" class="form-control" id="exampleInputEmail1" placeholder="Enter Name">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong><label for="exampleInputEmail1">Quantite</label></strong>
                <input type="text" name="quantite" class="form-control" id="exampleInputEmail1" placeholder="Enter Quantity">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong><label for="exampleInputEmail1">Prix</label></strong>
                <input type="text" name="prix" class="form-control" id="exampleInputEmail1" placeholder="Enter the Price">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>
   
</form>
@endsection



<!--
<div class="form-group">
    <label for="exampleInputEmail1">Name</label>
    <input type="text" class="form-control" name="name" id="exampleInputEmail1" placeholder="Enter Name">
  </div>
  
  <div class="form-group">
    <label for="exampleInputEmail1">Quantite</label>
    <input type="text" class="form-control" name="name" id="exampleInputEmail1" placeholder="Enter Quantity">
  </div>

  <div class="form-group">
    <label for="exampleInputEmail1">Prix</label>
    <input type="text" class="form-control" name="name" id="exampleInputEmail1" placeholder="Enter Price">
  </div>