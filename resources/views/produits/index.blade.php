@extends('layouts.app')
 
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Application de gestion de bar</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ url('/createpro') }}"> Create New PRODUIT</a>
            </div>
            <div class="pull-right">
                
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table class="table table-bordered">
        <tr>
            
            <th></th>
            <th>Name</th>
            <th>Quantite</th>
            <th>Prix</th>
            <th width="280px">Action</th>
        </tr>
        @foreach ($produits as $produit)
        <tr>
            
            <td>{{ ++$i }}</td>
            <td>{{ $produit->name }}</td>
            <td>{{ $produit->quantite }}</td>
            <td>{{ $produit->prix }}</td>
            <td>
                <form action="{{ route('deletepro',$produit->id) }}" method="POST">

                    <a class="btn btn-info" href="{{ route('showpro',$produit->id) }}">Show</a>

                    <a class="btn btn-primary" href="{{ route('editpro',$produit) }}">Edit</a>

                    @csrf
                    @method('DELETE')
  
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>

    {!! $produits->links() !!}
    
@endsection
