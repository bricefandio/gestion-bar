@extends('layouts.app')


@section('title')
 Management STUDENT| TIPAM
@endsection

@section('content')

<!-- ============ Body content start ============= -->

<div class="main-content-wrap sidenav-open d-flex flex-column">
    <div class="breadcrumb">
    <strong>Students</strong>
    </div>

    <div class="separator-breadcrumb border-top"></div>
    <div class="row">
        <div class="col-md-8 mb-8">
            <div class="card text-left">
                <div class="card-header">
                    <h5><strong>Students</strong> </h5>
                </div>

                <div class="card-body">
                        @include('inc.messages')
                    <div class="table-responsive">
                        <table id="demotable" class="display table table-striped table-bordered table-sm" style="width:100%">
                            <thead>
                                <tr>
                                    <th> quantite  </th>
                                    <th> prix </th>

                                    <th>Actions</th>
                                </tr>
                            </thead>
                                    
                            <tbody>
                                @foreach($students as $student)
                                    <tr>
                                      <td><strong>{{$student->quantite}}</strong></td>
                                      <td>{{$student->prix}}</td>
                                      
                                      <td>
                                       <a href="" data-toggle="modal" data-target="#edit{{$student->id}}"> <button type="button" class="btn btn-secondary btn-sm m-1">{{__('Edit')}}</button> </a>
                                        <a href="" data-toggle="modal" data-target="#delete{{$student->id}}"> <button type="submit" class="btn btn-danger">{{__('Delete')}}</button> </a>
                                      </td>
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th> quantite  </th>
                                    <th> prix </th>
                                    
                                    <th>Actions</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div> <!-- end table responsive -->
                              
                </div>  <!-- end card body -->
            </div> 
        </div>  <!-- end col-md-3 -->
        <!-- start the create Operator session -->
        <div class="col-lg-4 col-md-4">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h3>Add Students</h3>
                        </div>
                        <div class="card-body">
                            <form action="{{ route('create_student') }}" method="post">
                            @csrf
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="company_name">quantite</label>
                                            <input type="text" name="quantite" class="form-control" placeholder="Enter company Name" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="company_acronym">Prix</label>
                                            <input type="text" name="prix" class="form-control" placeholder="Company name" required>
                                        </div>
                                    </div>
                                    
                                </div>
                                
                            </div> <!-- end card body -->
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-3"></div>
                                    <div class="col-md-3"></div>
                                    <div class="col-md-3">
                                        <button type="submit" class="btn btn-primary btn-rounded btn-lg">Add</button>
                                    </div>
                                </div>
                            </div><!-- end card footer -->
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>  <!-- end col-md-9 -->
    </div> <!-- end row -->

</div>
  
<script>
$(document).ready(function() {
  $('#demotable').DataTable( {
      dom: 'Bfrtip',
      buttons: [
          'copy', 'csv', 'excel'
      ],
      "order": [],
      "paging": false

  } );
} );
</script>

<!-- start edit academic years modal -->
@foreach($students as $student)

 <div class="modal fade bd-example-modal-md" tabindex="-1" role="dialog" id= "edit{{$student->id}}" aria-labelledby="exampleModalCenterTitle" aria-hidden="false">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                </h5>
                    <h5 class="modal-title" id="exampleModalCenterTitle">Update the {{$student->quantite }} Student </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="card mb-4">
                            <div class="card-body">
                            <form method="post" action="{{route('update_student', $student->id) }}">
                                {{ csrf_field() }}
                                {{ method_field('PATCH') }}
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="quantity">quantite</label>
                                            <input type="text" name="quantite" class="form-control" value="{{$student->quantite}}" placeholder="Enter quantity" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="price">Prix</label>
                                            <input type="text" name="prix" class="form-control" value="{{$student->prix}}" placeholder="Enter price" required >
                                        </div>
                                    </div>
                                    
                                </div>
                                
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Update </button>
                </div>
                    </form>
            </div>
        </div>
    </div>
@endforeach
<!-- end edit academic years modal -->

<!-- start edit academic years modal -->
@foreach($students as $student)

 <div class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" id= "delete{{$student->id}}" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                </h5>
                    <h5 class="modal-title" id="exampleModalCenterTitle">Delete the {{$student->name }} School year </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="card mb-4">
                            <div class="card-body">
                            <form method="post" action="{{route('delete_student', $student->id) }}">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-12">
                                        <h2>Are you sure?</h2>
                                    </div>
                                </div>
                                   
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Yes, Proceed </button>
                </div>
                    </form>
            </div>
        </div>
    </div>
@endforeach
<!-- end edit academic years modal -->
    @endsection