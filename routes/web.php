<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
//Route::get('/produit/index', [App\Http\Controllers\ProduitsController::class, 'index'])->name('produits.index');
Route::get('/indexpro', [App\Http\Controllers\ProduitsController::class, 'index'])->name('indexpro');

//Route::get('/produit/create', [App\Http\Controllers\ProduitsController::class, 'create'])->name('produits.create');
Route::get('/createpro', [App\Http\Controllers\ProduitsController::class, 'create']);

//Route::post('/produit/store', [App\Http\Controllers\ProduitsController::class, 'store'])->name('produits.store');
Route::post('/storepro', [App\Http\Controllers\ProduitsController::class, 'store'])->name('storepro');

//Route::get('/produit/show', [App\Http\Controllers\ProduitsController::class, 'show'])->name('produits.show');
Route::get('/showpro/{produit}', [App\Http\Controllers\ProduitsController::class, 'show'])->name('showpro');

Route::get('/editpro/{produit}', [App\Http\Controllers\ProduitsController::class, 'edit'])->name('editpro');

//Route::post('/produit/update', [App\Http\Controllers\ProduitsController::class, 'update'])->name('produits.update');
Route::put('/updatepro/{produit}', [App\Http\Controllers\ProduitsController::class, 'update'])->name('updatepro');

Route::delete('/deletepro/{produit}', [App\Http\Controllers\ProduitsController::class, 'destroy'])->name('deletepro');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');;
//road vehicle
Route::get('/about', [App\Http\Controllers\ProduitsController::class, 'about'])->name('about');
Route::post('/createvehicle', [App\Http\Controllers\ProduitsController::class, 'createvehicle']);

// students template

Route::get('student',[App\Http\Controllers\HomeController::class, 'indexStudent'])->name('view_student');
Route::post('create/student',[App\Http\Controllers\HomeController::class, 'createStudent'])->name('create_student');
Route::patch('update/student/{id}',[App\Http\Controllers\HomeController::class, 'updateStudent'])->name('update_student');
Route::post('delete/student/{id}',[App\Http\Controllers\HomeController::class, 'deleteStudent'])->name('delete_student');


